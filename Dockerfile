FROM gradle:7.1.0-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11


RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/alliance-0.0.1-SNAPSHOT.jar /app/alliance-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/app/alliance-0.0.1-SNAPSHOT.jar"]