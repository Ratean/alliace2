insert into alliance.public.customers (firstname, lastname)
values ('Spartak', 'Martirosyan'),
       ('Maksim', 'Maksimov');

insert into alliance.public.contacts (contact, contact_type, customer_id)
values ('89990002233', 'PHONE_NUMBER', 1),
('ratean', 'TELEGRAM', 1),
('es_hay_em@list.ru', 'EMAIL', 1),
('es_hay_em@list.ru', 'EMAIL', 2);