--liquibase formatted sql

--changeset smartirosyan:1
alter table alliance.public.billboard
add max_amount_canvas int,
ADD    latitude decimal,
ADD    lngitude decimal,
ADD    location_description varchar,
ADD    billboard_type varchar(10),
ADD    canvas_type varchar(10),
drop constraint billboard_address_key;

alter table alliance.public.canvases
drop column is_front,
add notification_date date,
alter column start_date TYPE date,
alter column end_date TYPE date;

--чтобы они добавились в миграцию