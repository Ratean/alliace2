--liquibase formatted sql

--changeset smartirosyan:1
create table if not exists public.customers
(
    id        BIGSERIAL PRIMARY KEY,
    firstname varchar(55),
    lastname  varchar(55)
);

create table if not exists public.billboard
(
    id      BIGSERIAL PRIMARY KEY,
    address varchar unique not null
);

create table if not exists public.canvases
(
    id           BIGSERIAL PRIMARY KEY,
    start_date   timestamp,
    end_date     timestamp,
    is_front     boolean,
    billboard_id bigint references billboard (id)
);

create table if not exists public.contacts
(
    id           BIGSERIAL PRIMARY KEY,
    contact      varchar,
    contact_type varchar(15),
    customer_id  bigint references customers (id)
);

create table if not exists public.promotional_products
(
    id          BIGSERIAL PRIMARY KEY,
    customer_id bigint references customers (id) not null,
    is_archive  boolean
);

--чтобы они добавились в миграцию