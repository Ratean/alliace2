package com.alliance.service.abstracts;

import com.alliance.models.dto.CustomerDto;

public interface NotificationService {
    void send(CustomerDto customerDto);
}
