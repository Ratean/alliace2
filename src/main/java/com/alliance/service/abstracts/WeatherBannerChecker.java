package com.alliance.service.abstracts;

import com.alliance.eception.WeatherApiException;

public interface WeatherBannerChecker {
    boolean isWeatherGood(double latitude, double longitude, int workingTime) throws WeatherApiException;
}
