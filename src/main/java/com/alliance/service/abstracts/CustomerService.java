package com.alliance.service.abstracts;

import com.alliance.models.dto.CanvasDto;
import com.alliance.models.dto.CustomerDto;
import com.alliance.models.entity.Customer;

import java.util.List;

public interface CustomerService {
    void register(CustomerDto customerDto);
    void notifyBeforeExpirationDate(CustomerDto customerDto, CanvasDto canvasDto);
    Customer save(CustomerDto customerDto);
    CustomerDto findById(long id);
    List<CustomerDto> getAllCustomers();
}
