package com.alliance.service.Impl;

import com.alliance.converters.CustomerMapper;
import com.alliance.dao.CustomerDAO;
import com.alliance.models.dto.CanvasDto;
import com.alliance.models.dto.CustomerDto;
import com.alliance.models.entity.Customer;
import com.alliance.service.abstracts.CustomerService;
import com.alliance.service.abstracts.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerDAO customerDAO;
    private final NotificationService notificationService;
    private final CustomerMapper mapper;

    @Override
    public void register(CustomerDto customerDto) {
        notificationService.send(customerDto);
    }

    @Override
    public void notifyBeforeExpirationDate(CustomerDto customerDto, CanvasDto canvasDto) {
        //логику продумать предупреждения заранее
        //Schedule Spring???
        //тест
        notificationService.send(customerDto);
    }

    @Override
    public Customer save(CustomerDto customerDto) {
        return customerDAO.save(mapper.toModel(customerDto));
    }

    @Override
    public CustomerDto findById(long id) {
        return mapper.toDto(customerDAO.findById(id).orElse(new Customer()));
    }


    @Override
    public List<CustomerDto> getAllCustomers() {
        return mapper.toDto(customerDAO.findAll());
    }
}
