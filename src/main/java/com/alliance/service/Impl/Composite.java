package com.alliance.service.Impl;

import com.alliance.models.dto.CustomerDto;
import com.alliance.service.abstracts.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Primary
@RequiredArgsConstructor
public class Composite implements NotificationService {

    private final List<NotificationService> services;

    @Override
    public void send(CustomerDto customerDto) {
        for (NotificationService service : services) {
            service.send(customerDto);
            log.info(service.getClass().getName() + " был вызвал в цикле");
        }
        log.info("Логика в Composite");
    }
}
