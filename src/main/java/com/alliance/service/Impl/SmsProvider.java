package com.alliance.service.Impl;

import com.alliance.models.dto.CustomerDto;

import com.alliance.service.abstracts.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SmsProvider implements NotificationService {
    @Override
    public void send(CustomerDto customerDto) {
        log.info("Отправляем смс");
    }
}
