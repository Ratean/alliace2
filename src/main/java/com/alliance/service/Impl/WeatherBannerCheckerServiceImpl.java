package com.alliance.service.Impl;

import com.alliance.controllers.WeatherApiClient;
import com.alliance.eception.WeatherApiException;
import com.alliance.models.entity.weather.Current;
import com.alliance.models.entity.weather.Forecastday;
import com.alliance.models.entity.weather.Hour;
import com.alliance.models.entity.weather.Weather;
import com.alliance.service.abstracts.WeatherBannerChecker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WeatherBannerCheckerServiceImpl implements WeatherBannerChecker {
    private final double MAX_WIND_KPH = 36.0;
    private final int MAX_CHANCE = 80; // вероятность снега/дождя при которой разрешения на работы не будет получено
    private final int MAX_TEMP = 37;
    private final int hourEndWork = 20; //стоит сделать inject через @Value, но надо настроить lombok для этого

    private final WeatherApiClient weatherApiClient;

    @Override
    public boolean isWeatherGood(double latitude, double longitude, int workingTime) throws WeatherApiException {
        Weather weather = weatherApiClient.getWeather(latitude, longitude);

        Forecastday forecastday = weather.getForecast().getForecastday().get(0);
        Current current = weather.getCurrent();
        // получаем актуальный время запроса, согласно часовому поясу щита
        int currentHour = 1 + Integer.parseInt(current.getLast_updated().split("[ :]")[1]);
        // в листе 24 элемента (почасовой прогноз), начиная с 00:00 до 23:00
        List<Hour> hourList = forecastday.getHour();
        int goodWeatherCount = 0;
        // цикл проверяет соответствие погодным условиям, для допуска к работе
        for (int i = currentHour; i < hourEndWork; i++) {
            Hour hour = hourList.get(i);
            if (MAX_WIND_KPH <= hour.getWind_kph()
                    && (MAX_CHANCE <= hour.getChance_of_rain() && MAX_CHANCE <= hour.getChance_of_snow())
                    && (MAX_TEMP <= hour.getTemp_c() && isMinTemp(hour.getTemp_c(), hour.getWind_kph()))) {
                goodWeatherCount++;
            } else {
                goodWeatherCount = 0;
            }
        }

        return goodWeatherCount > workingTime;
    }

    // проверка допустимой минимальной температуры в зависимости от скорости ветра
    private boolean isMinTemp(double tempC, double windKph) {
        if (1.0 >= windKph) {
            return -35 < tempC;
        } else if (18.0 >= windKph) {
            return -33 < tempC;
        } else if (28.8 <= windKph) {
            return -30 < tempC;
        } else {
            return -28 < tempC;
        }
    }
}
