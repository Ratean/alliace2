package com.alliance.converters;

import com.alliance.models.dto.CustomerDto;
import com.alliance.models.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);
    CustomerDto toDto(Customer customer);
    Customer toModel(CustomerDto customerDto);

    default List<CustomerDto> toDto(List<Customer> list) {
        return list.stream().map(this::toDto).collect(Collectors.toList());
    }

    default List<Customer> toModel(List<CustomerDto> list) {
        return list.stream().map(this::toModel).collect(Collectors.toList());
    }
}
