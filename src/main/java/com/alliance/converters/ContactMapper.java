package com.alliance.converters;

import com.alliance.models.dto.ContactDto;
import com.alliance.models.entity.Contact;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ContactMapper {
    ContactMapper INSTANCE = Mappers.getMapper(ContactMapper.class);
    ContactDto toDto(Contact contact);
    Contact toModel(ContactDto contactDto);
}
