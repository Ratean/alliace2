package com.alliance.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "billboards")
public class Billboard {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "billboard_seq")
    @SequenceGenerator(name = "billboard_seq",
            sequenceName = "billboard_id_seq", allocationSize = 1)
    private long id;
    private int maxAmountCanvas;
    private double latitude;
    private double longitude;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "billboard_id")
    private List<Canvas> canvases;
    private String address;
    private String locationDescription;
    @Enumerated(EnumType.STRING)
    private BillboardType billboardType;
    @Enumerated(EnumType.STRING)
    private CanvasType canvasType;

}
