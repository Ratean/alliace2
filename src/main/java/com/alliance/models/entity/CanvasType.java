package com.alliance.models.entity;

public enum CanvasType {
    CLASSICAL,
    PRIZMATRON,
    DIGITAL
}
