package com.alliance.models.entity;

public enum BillboardType {
    CLASSICAL,
    SUPERBOARD,
    CITYBOARD
}
