package com.alliance.models.entity.weather;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Hour {
    private String time;
    private double temp_c;
    private double wind_kph;
    private double gust_kph;
    private int chance_of_rain;
    private int chance_of_snow;
}
