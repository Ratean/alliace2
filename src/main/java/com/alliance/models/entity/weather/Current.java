package com.alliance.models.entity.weather;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Current {
    private String last_updated;
    private double temp_c;
    private double wind_kph;
}
