package com.alliance.models.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "canvases")
public class Canvas {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "canvases_seq")
    @SequenceGenerator(name = "canvases_seq",
            sequenceName = "canvases_id_seq", allocationSize = 1)
    private long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate notificationDate;
    public LocalDate getNotificationDate() {
        notificationDate = endDate.minusDays(2L);
        return notificationDate;
    }
}
