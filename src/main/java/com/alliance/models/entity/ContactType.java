package com.alliance.models.entity;

public enum ContactType {
    PHONE_NUMBER,
    EMAIL,
    TELEGRAM,
}
