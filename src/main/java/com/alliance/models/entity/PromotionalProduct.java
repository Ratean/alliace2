package com.alliance.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "promotional_products")
public class PromotionalProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "promotional_products_seq")
    @SequenceGenerator(name = "promotional_products_seq",
            sequenceName = "promotional_products_id_seq", allocationSize = 1)
    private long id;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "promotional_product")
    private List<Canvas> canvasList;
    private boolean isArchive = false;
}
