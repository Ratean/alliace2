package com.alliance.models.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "contacts_seq")
    @SequenceGenerator(name = "contacts_seq",
            sequenceName = "contacts_id_seq", allocationSize = 1)
    private long id;
    private String contact;
    @Enumerated(EnumType.STRING)
    private ContactType contactType;
}
