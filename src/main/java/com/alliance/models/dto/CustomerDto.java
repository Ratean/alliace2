package com.alliance.models.dto;

import com.alliance.models.entity.Contact;
import com.alliance.models.entity.PromotionalProduct;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.OneToMany;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class CustomerDto {
    private long id;
    private String firstname;
    private String lastname;
    private List<ContactDto> contacts;
    private List<PromotionalProduct> productList;
}
