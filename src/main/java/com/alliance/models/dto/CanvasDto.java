package com.alliance.models.dto;

import com.alliance.models.entity.Billboard;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class CanvasDto {
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private Billboard billboard;
}
