package com.alliance.models.dto;

import com.alliance.models.entity.ContactType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class ContactDto {
    private long id;
    private String contact;
    private ContactType contactType;
}
