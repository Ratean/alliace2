package com.alliance.controllers;

import com.alliance.eception.WeatherApiException;
import com.alliance.models.entity.weather.Weather;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
public class WeatherApiClient {
    private final String URI = "http://api.weatherapi.com/v1/forecast.json?key=";
    private final String keyUrl = "3dbe635e3f8c47b2a62164316232601"; // 3dbe635e3f8c47b2a62164316232601
    private final String latitudeAndLongitudeUrl = "&q=";
    private final String daysUrl = "&days=1";

    public Weather getWeather(double latitude, double longitude) throws WeatherApiException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Weather> response = restTemplate.getForEntity(URI + keyUrl + latitudeAndLongitudeUrl +
                latitude + "," + longitude + daysUrl, Weather.class);
        HttpStatus httpStatus = response.getStatusCode();
        if (!httpStatus.is2xxSuccessful()) {
            throw new WeatherApiException();
        }
        return response.getBody();
    }
}
