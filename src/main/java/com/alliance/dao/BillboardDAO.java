package com.alliance.dao;

import com.alliance.models.entity.Billboard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillboardDAO extends JpaRepository<Billboard, Long> {
}
