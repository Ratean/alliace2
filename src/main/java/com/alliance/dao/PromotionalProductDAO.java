package com.alliance.dao;

import com.alliance.models.entity.PromotionalProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotionalProductDAO extends JpaRepository<PromotionalProduct, Long> {
}
