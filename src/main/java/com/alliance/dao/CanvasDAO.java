package com.alliance.dao;

import com.alliance.models.entity.Canvas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CanvasDAO extends JpaRepository<Canvas, Long> {
}
