package com.alliance.controllers;

import com.alliance.eception.WeatherApiException;
import com.alliance.models.entity.weather.Weather;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class WeatherApiClientTest {

    @Test
    void testWeatherApi() throws WeatherApiException {
        WeatherApiClient weatherApiClient = new WeatherApiClient();
        Weather weather = weatherApiClient.getWeather(40.5587, 10.8565);
        assertEquals(weather.getForecast().getForecastday().get(0).getDate().getClass(), LocalDate.class);
    }
}
